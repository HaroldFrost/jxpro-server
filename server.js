/**
 * Created by harold on 30/12/16.
 */
var express = require('express');
var barrier = require('./barrier');
var Bear = require('./model/bear.js');
var bodyParser = require('body-parser');
var config = require('./config/config');
var loginRoute = require('./routes/loginRoute');
var logger = require('morgan');
var mongoose = require('mongoose');
var repairRoute = require('./routes/repairOrderRoute');
var staffRoute = require('./routes/staffRoute');
var router = express.Router();
var port = config.server.port;

var app = express();
mongoose.connect(config.db.url);

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// REGISTER OUR ROUTES --------------------
// all of our routes will be prefixed with /api
var routePrefix = '/api';

// Morgan middleware to log every request to the console
app.use(logger('dev'));

// Routes for root url
app.use(routePrefix, barrier.enableCORS, router, loginRoute);

// Routes for staff
app.use(routePrefix + '/staff/', barrier.enableCORS, staffRoute);

// Routes for repair orders
app.use(routePrefix + '/repair/', barrier.enableCORS, repairRoute);


// BASE SETUP ---------------------


// Start the server --------------------------
app.listen(port, function(req, res){
    console.log('Listening on port: ' + port);
});
