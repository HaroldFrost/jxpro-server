var express = require('express');
var router = express.Router();
var Staff = require('../model/Staff');
var barrier = require('../barrier');
var UserSession = require('../model/UserSession');
var bcrypt = require('bcrypt-nodejs');
const saltRounds = 10;

/**
 * Find a staff from the database, by the token provided in the req
 * @param req
 * @returns {Promise}
 */
var findStaffByToken = function(req){
    return new Promise(function(resolve, reject){
        UserSession.findOne({
            token: req.header('Authorization').split('Bearer ')[1]
        }).populate('staff').exec().then(function(userSession){
            console.log("resolve");
            resolve(userSession.staff);
        }, function(reason){
            // TODO: log to the error log file
            console.log("reject");
            reject(reason);
        });
    });
};

/**
 * Change password
 */
router.route('/changePassword/').post(barrier.requireLogin, function(req, res){
    if (req.body.oldPassword && req.body.newPassword){
        console.log(req.body);
    }else{
        console.log(req.body);
        res.status(400).send("Invalid input");
        return;
    }

    findStaffByToken(req).then(function(staff){
        if (!staff){
            res.status(404).send("Cannot find staff");
            return;
        }

        var oldPassword = req.body.oldPassword;
        var newPassword = req.body.newPassword;

        if (bcrypt.compareSync(oldPassword, staff.password)){
            var salt = bcrypt.genSaltSync(saltRounds);
            staff.password = bcrypt.hashSync(newPassword, salt);

            staff.save().then(function(staff){
                res.status(200).send("保存成功！");
            }, function (reason) {
                res.status(500).send("保存失败！原因: " + reason);
            });
        }else{
            res.status(400).send("旧密码不相符。");
        }

    }, function(reason){
        res.status(500).send("发生未知错误。原因： "+ reason);
    });
});

module.exports = router;