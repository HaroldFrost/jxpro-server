/**
 * Created by harold on 4/1/17.
 */
var express = require('express');
var router = express.Router();
var UserSession = require('../model/UserSession');
var Staff = require('../model/Staff');
var srs = require('secure-random-string');
var barrier = require('../barrier');
var bcrypt = require('bcrypt-nodejs');
const saltRounds = 10;

/**
 * Find a staff id ONLY from the database, by the token provided in the req
 * @param req
 * @returns {Promise}
 */
var findStaffIdByToken = function(req){
    return new Promise(function(resolve, reject){
        UserSession.find({
            token: req.header('Authorization').split('Bearer ')[1]
        }).exec().then(function(userSession){
            resolve(userSession[0].staff);
        }, function(reason){
            // TODO: log to the error log file
            reject(reason);
        });
    });
};


/**
 * Route for login
 * Check the received staff code and password are valid
 * generate a token to be stored in the database if the valid combination is provided
 * otherwise, return 401
 *
 * For preventing double login, it checks if the staff has an existing token in the database.
 *
 * The token expires in 2 hours after creation but will be renewed if the staff login again
 */
router.route('/login').post(function(req, res){
    var staffCode = req.body.code;
    var plainTextPassword = req.body.password;
    // Check if the code and password are correct
    Staff.findOne({
        code: staffCode
    }, function(err, staff){
        // Incorrect combination
        if (!staff){
            res.status(401).json({
                message: "Staff not found."
            });
        }else{
            if (!bcrypt.compareSync(plainTextPassword, staff.password)){
                res.status(401).json({
                    message: "Invalid combination of staff code and password"
                });
                return;
            }


            // If the combination is correct, try to find an existing token of that staff
            UserSession.findOne({
                staff: staff._id
            }, function(err, userSession){

                // If there is no existing token, generate one with 64 random string
                if (!userSession){
                    // create a new userSession
                    var token = srs({
                        length: 512
                    });
                    var newUserSession = new UserSession();
                    newUserSession.staff = staff._id;

                    // Setting the expire time to 2 hours later
                    newUserSession.expireAt = new Date();
                    newUserSession.expireAt.setTime(newUserSession.expireAt.getTime() + (2*60*60*1000));
                    newUserSession.token = token;

                    // Save the token to the database
                    newUserSession.save(function(err){
                        if (err) {
                            res.status(500).send(err);
                        }
                        else{
                            res.status(200).json({
                                message: "Login successfully!",
                                staff: {
                                    token: token,
                                    code: staff.code,
                                    name: staff.name,
                                    role: staff.role
                                }
                            });
                        }
                    });
                }
                else{
                    // It token exists, update the expire time and return the token
                    userSession.expireAt.setTime(Date.now() + (2*60*60*1000));
                    userSession.markModified('expireAt');

                    // Update the token in the database
                    userSession.save(function(err){
                        if (err){
                            res.status(500).send(err);
                        }
                        else{
                            res.status(200).json({
                                message: "Token renewed! Welcome back!",
                                staff: {
                                    token: userSession.token,
                                    code: staff.code,
                                    name: staff.name,
                                    role: staff.role
                                }
                            });
                        }
                    });


                }
            });
        }
    });
});



/**
 * Route for registering staff
 * TODO: delete or add security mechanism on production
 */
router.route('/register').post(function(req, res){
    var staff = new Staff();
    staff.code = req.body.code;
    staff.password = req.body.password;
    staff.name = req.body.name;
    staff.role = req.body.role;

    var salt = bcrypt.genSaltSync(saltRounds);
    staff.password = bcrypt.hashSync(staff.password, salt);
    staff.save(function (err) {
        if (err)
            res.status(500).json(err);
        else{
            res.json({
                message: 'Staff created!'
            });
        }
    })
});

/**
 * Route for logout
 */
router.route('/logout').post(barrier.requireLogin, function(req, res){
    // find the staff first to retrieve its id
    Staff.find({
        code: req.body.code
    }, function(err, staff){
        // if an error happens
        if (err){
            res.status(500).json({message: err});
        }else{
            // if the staff is not found, return 404
            if (!staff[0]){
                res.status(404).json({
                    message: "Cannot find staff"
                });
            }else{
                // delete the token in the database
                var token = req.get('Authorization').split('Bearer ')[1];
                UserSession.remove({
                    staff: staff[0]._id,
                    token: token
                }, function(err, docsRemoved){
                    if (err){
                        res.status(500).json({message: err});
                    }
                    else{
                        // Check if there is something deleted
                        if (docsRemoved.result.n === 0){
                            res.status(500).json({
                                message: "The token was not found."
                            });
                        }else{
                            // Logout if the token is successfully deleted
                            res.status(200).json({
                                message: "Logout successfully."
                            });
                        }
                    }
                });
            }
        }
    });
});




module.exports = router;
