/**
 * Created by harold on 4/1/17.
 */
var express = require('express');
var router = express.Router();
var RepairOrder = require('../model/RepairOrder');
var Staff = require('../model/Staff');
var UserSession = require('../model/UserSession');
var barrier = require('../barrier');
var formidable = require('formidable');

/**
 * Handles saving an order, also return a response,
 * with a specified success message and error message
 * @param repairOrder
 * @param successMessage
 * @param errorMessage
 * @param res
 */
var saveOrder = function(repairOrder, successMessage, errorMessage, res){
    // Save the order to the database
    repairOrder.save().then(function(repairOrder){

        res.status(200).json({
            message: successMessage,
            id : repairOrder._id
        });
    }, function(reason){
        // TODO: log to the error log file
        console.log(reason);
        res.status(500).send(errorMessage);
    });
};

/**
 * Find a staff id ONLY from the database, by the token provided in the req
 * @param req
 * @returns {Promise}
 */
var findStaffIdByToken = function(req){
    return new Promise(function(resolve, reject){
        UserSession.find({
            token: req.header('Authorization').split('Bearer ')[1]
        }).exec().then(function(userSession){
            resolve(userSession[0].staff);
        }, function(reason){
            // TODO: log to the error log file
            reject(reason);
        });
    });
};

/**
 * 落单接口
 * route for create a repair order
 */
router.route('/').post(barrier.requireLogin, function(req, res){
    if (req.body.client && req.body.description){

    }else{
        return res.status(400).send("缺少字段。");
    }

    var repairOrder = new RepairOrder();

    findStaffIdByToken(req).then(function(staffId){
        // Set the servedBy to the correspond staff
        repairOrder.servedBy = staffId;

        // Set the information
        repairOrder.client = req.body.client;

        if (req.body.phone){
            repairOrder.phone = req.body.phone;
        }

        repairOrder.description = req.body.description;
        repairOrder.date = Date.now();

        // Check if there are a quote and a cost
        var quote = req.body.quote;
        var cost = req.body.cost;

        repairOrder.quote = quote || 0;

        repairOrder.cost = req.body.cost || 0;

        var successMessage = "Successfully placed order";
        var errorMessage = "Something went wrong with saving the order.";
        saveOrder(repairOrder, successMessage, errorMessage, res);
    }, function(reason){
        // TODO: log to the error log file
        console.log(reason);
        res.status(500).send("Something wrong with finding order list in the server");
    });
});

/**
 * 日期列表接口
 * Route for retrieving the list of all orders from database
 */
router.route('/list/').post(barrier.requireLogin, function(req, res){
    var dateStart = new Date(req.body.dateStart);
    var dateEnd = new Date(req.body.dateEnd);

    RepairOrder.find({date: {$gt :dateStart, $lt: dateEnd}}).populate('servedBy').populate('technician').populate('cashier').sort({date: -1}).exec(function(err, repairOrders){
        if (err){
            // TODO: log to the error log file
            res.status(500).send("Something wrong with finding order list in the server");
        }else{
            for (var i = 0; i < repairOrders.length; i++){
                repairOrders[i].servedBy.password = undefined;
                repairOrders[i].servedBy.code = undefined;
                // repairOrders[i].servedBy.id = undefined;
                // repairOrders[i].servedBy.role = undefined;
                if (repairOrders[i].technician){
                    repairOrders[i].technician.code = undefined;
                    repairOrders[i].technician.password = undefined;
                }

                if (repairOrders[i].cashier){
                    repairOrders[i].cashier.code = undefined;
                    repairOrders[i].cashier.password = undefined;
                }
            }
            res.status(200).json(repairOrders);
        }
    });
});

/**
 * 日期列表接口
 * Route for retrieving the list of all orders from database
 */
router.route('/audit/list/').post(barrier.requireLogin, function(req, res){
    var dateStart = req.body.dateStart;
    var dateEnd = req.body.dateEnd;

    RepairOrder.find({paid: true, paymentDate: {$gt :dateStart, $lt: dateEnd}}).populate('servedBy').populate('technician').populate('cashier').sort({paymentDate: -1}).exec(function(err, repairOrders){
        if (err){
            // TODO: log to the error log file
            res.status(500).send("Something wrong with finding order list in the server");
        }else{
            for (var i = 0; i < repairOrders.length; i++){
                repairOrders[i].servedBy.password = undefined;
                repairOrders[i].servedBy.code = undefined;
                // repairOrders[i].servedBy.id = undefined;
                // repairOrders[i].servedBy.role = undefined;
                if (repairOrders[i].technician){
                    repairOrders[i].technician.code = undefined;
                    repairOrders[i].technician.password = undefined;
                }

                if (repairOrders[i].cashier){
                    repairOrders[i].cashier.code = undefined;
                    repairOrders[i].cashier.password = undefined;
                }
            }
            res.status(200).json(repairOrders);
        }
    });
});

/**
 * 翻页列表接口
 * Route for retrieving the list of all orders from database
 */
router.route('/page/:page').get(barrier.requireLogin, function(req, res){
    var skipNumber = req.params.page;
    if (skipNumber < 1){
        skipNumber = 1;
    }
    var pageSize = 30;
    skipNumber--;
    skipNumber *= pageSize;

    RepairOrder.find().skip(skipNumber).limit(pageSize).populate('servedBy').populate('technician').sort({date: -1}).exec(function(err, repairOrders){
        if (err){
            // TODO: log to the error log file
            res.status(500).send("Something wrong with finding order list in the server");
        }else{
            for (var i = 0; i < repairOrders.length; i++){
                repairOrders[i].servedBy.password = undefined;
                repairOrders[i].servedBy.code = undefined;
                // repairOrders[i].servedBy.id = undefined;
                // repairOrders[i].servedBy.role = undefined;
                if (repairOrders[i].technician){
                    repairOrders[i].technician.code = undefined;
                    repairOrders[i].technician.password = undefined;
                }
            }
            res.status(200).json(repairOrders);
        }
    });
});

/**
 * 全部列表接口
 */
router.route('/all/').post(barrier.requireLogin, function(req, res){
    RepairOrder.find().populate('servedBy').populate('technician').sort({date: -1}).exec(function(err, repairOrders){
        if (err){
            // TODO: log to the error log file
            res.status(500).send("Something wrong with finding order list in the server");
        }else{
            for (var i = 0; i < repairOrders.length; i++){
                repairOrders[i].servedBy.password = undefined;
                repairOrders[i].servedBy.code = undefined;
                // repairOrders[i].servedBy.id = undefined;
                // repairOrders[i].servedBy.role = undefined;
                if (repairOrders[i].technician){
                    repairOrders[i].technician.code = undefined;
                    repairOrders[i].technician.password = undefined;
                }
            }

            res.status(200).json(repairOrders);
        }
    });
});

/**
 * 获取单张订单接口
 * Route for retrieving a single order
 */
router.route('/:order_id').get(barrier.requireLogin, function(req, res){
    var repairPromise = RepairOrder.findById(req.params.order_id).populate('servedBy').populate('technician').populate('cashier').exec();

    repairPromise.then(function(repairOrder){
        if (repairOrder){
            repairOrder.servedBy.password = undefined;
            repairOrder.servedBy.code = undefined;

            if (repairOrder.technician){
                repairOrder.technician.password = undefined;
                repairOrder.technician.code = undefined;
            }

            if (repairOrder.cashier){
                repairOrder.cashier.password = undefined;
                repairOrder.cashier.code = undefined;
            }

            res.status(200).json(repairOrder);
        }else{
            res.status(404).send("Cannot find this requested order");
        }
    }, function(reason){
        res.status(500).send("Something wrong in the server while retrieving the order.");

        // TODO: log to the error log file
        console.log(reason);
    });
});

/**
 * 更新订单接口
 * Route for changing detail of an order
 */
router.route('/:order_id').put(barrier.requireLogin, barrier.archivedOrderBarrier, function(req, res){
    if (req.body.client && req.body.description){

    }else{
        return res.status(400).send("缺少字段。");
    }

    RepairOrder.findById(req.params.order_id).exec().then(function(repairOrder){
        if (repairOrder){
            // TODO: log the update record to the log file

            repairOrder.client = req.body.client;

            repairOrder.phone = req.body.phone || '';

            repairOrder.quote = req.body.quote || 0;
            repairOrder.description = req.body.description;
            repairOrder.cost = req.body.cost;

            // Save the order to the database
            var successMessage = "Successfully updated order";
            var errorMessage = "Something went wrong with saving the order.";

            saveOrder(repairOrder, successMessage, errorMessage, res);

        }else{
            res.status(404).send("Cannot find this requested order");
        }
    }, function(reason){
        res.status(500).send("Something wrong in the server while retrieving the order.");

        // TODO: log to the error log file
        console.log(reason);
    });
});

/**
 * 收款接口
 * Route for receiving payment of an order
 */
router.route("/pay/").post(barrier.requireCashier, barrier.archivedOrderBarrier, function(req, res){
    // validate the request first, to see if it contains the required fields
    if (req.body.id && req.body.paymentMethod && req.body.receipt){

    }else{
        res.status(400).send("Invalid input.");
    }

    RepairOrder.findById(req.body.id).exec().then(function(repairOrder){
        if (repairOrder){
            // TODO: log the update record to the log file

            if (repairOrder.paid){
                res.status(400).send("Order already paid.");
            }

            if (req.body.receipt > 0){
                repairOrder.receipt = req.body.receipt;
                repairOrder.paymentDate = new Date();
                repairOrder.paymentMethod = req.body.paymentMethod;
                repairOrder.paid = true;
                repairOrder.markModified("paid");

                findStaffIdByToken(req).then(function(staffId){
                    repairOrder.cashier = staffId;
                    var successMessage = "Successfully updated payment";
                    var errorMessage = "Something went wrong with saving the order.";
                    saveOrder(repairOrder, successMessage, errorMessage, res);
                }, function(reason){

                    // TODO: log to the error log file
                    console.log(reason);

                    res.status(500).send("Something went wrong with retrieving the staff id.");
                });
            }else{
                res.status(400).send("Invalid input.");
            }
        }else{
            res.status(404).send("Cannot find this requested order");
        }
    }, function(reason){
        res.status(500).send("Something wrong in the server while retrieving the order.");

        // TODO: log to the error log file
        console.log(reason);
    });
});

/**
 * 销单接口
 * Route for archiving an order
 */
router.route('/archive/').post(barrier.requireAdmin, barrier.archivedOrderBarrier, function(req, res){
    if (req.body.id){

    }else{
        res.status(400).send("Invalid input.");
    }

    RepairOrder.findById(req.body.id).exec().then(function(repairOrder){
        if (!repairOrder){
            res.status(404).send("Order not found.");
        }

        if (repairOrder.paid && repairOrder.repairStatus == 1 && repairOrder.passedAudit){
            repairOrder.archived = true;
            repairOrder.markModified('archived');
            saveOrder(repairOrder, "Successfully archived order.", "Something went wrong with saving the order", res);
        }else{
            // res.status(400).send("The order is not completed/paid/audited yet.");
            res.status(400).send("订单还没有达到要求。");
        }

    }, function(reason){
        res.status(500).send("Something wrong in the server while saving the order.");

        // TODO: log to the error log file
        console.log(reason);
    });
});

/**
 * 分配接口
 */
router.route('/assign/').post(barrier.requireLogin, function(req, res) {
    if (req.body.id){

    }else{
        res.status(400).send('Invalid input.');
    }

    RepairOrder.findById(req.body.id).exec().then(function(repairOrder){

        if (repairOrder && !repairOrder.technician){
            findStaffIdByToken(req).then(function(staff){
                repairOrder.technician = staff;
                saveOrder(repairOrder, "Successfully assigned to staff.", "Something went wrong while saving", res);
            }, function (reason) {
                console.log(reason);
                res.status(500).send("Something went wrong with retrieving the staff");
            });
        }else{
            res.status(400).send("Order not found or assigned already.");
        }
    }, function (reason) {
        console.log(reason);
    });
});

/**
 * 维修完成接口
 */
router.route('/complete/').post(barrier.requireLogin, barrier.archivedOrderBarrier, function(req, res){
    if (req.body.id){

    }else{
        res.status(400).send("Invalid input.");
    }

    RepairOrder.findById(req.body.id).exec().then(function(repairOrder){
        findStaffIdByToken(req).then(function(staffId) {
            if (repairOrder){

                if (repairOrder.technician){
                    if (staffId.toString() === repairOrder.technician.toString()){
                        repairOrder.repairStatus = 1;
                        saveOrder(repairOrder, "Successfully mark order completed.", "Something went wrong with saving the order", res);
                    }else{
                        res.status(403).send("你不是这张单的维修员。");
                    }

                }else{
                    res.status(400).send("Order not assigned to a technician.");
                }

            }else{
                res.status(404).send("找不到订单！");
            }
        });



    }, function (reason) {
        res.status(500).send("Something wrong in the server while saving the order.");

        // TODO: log to the error log file
        console.log(reason);
    });
});

/**
 * 查账接口
 */
router.route('/audit/').post(barrier.requireAdmin, function(req, res){
    if (req.body.id){

    }else{
        res.status(400).send("Invalid input.");
    }

    RepairOrder.findById(req.body.id).exec().then(function(repairOrder){
        if (!repairOrder){
            res.status(404).send("Order not found.");
        }else if (repairOrder.passedAudit){
            res.status(400).send("你已经查过账了。")
        }else{
            repairOrder.passedAudit = true;
            repairOrder.markModified('passedAudit');
            saveOrder(repairOrder, "Successfully mark order audited.", "Something went wrong with saving the order", res);
        }
    }, function (reason) {
        res.status(500).send("Something wrong in the server while saving the order.");

        // TODO: log to the error log file
        console.log(reason);
    });
});

/**
 * 送货单 调拨单接口
 */
router.route('/attach/').post(barrier.requireLogin, barrier.archivedOrderBarrier, function(req, res){
    if (req.body.id && (req.body.deliveryBill || req.body.requisitionBill)){

    }else{
        res.status(400).send("Invalid input.");
    }

    RepairOrder.findById(req.body.id).exec().then(function(repairOrder){
        if (repairOrder){
            if (req.body.deliveryBill && !repairOrder.deliveryBill.code){
                repairOrder.deliveryBill = req.body.deliveryBill;
                saveOrder(repairOrder, "成功添加送货单！", "Something wrong again.", res);
            }else if(req.body.requisitionBill){
                repairOrder.requisitionBill.push(req.body.requisitionBill);
                saveOrder(repairOrder, "成功添加调拨单！", "Something wrong again.", res);
            }else{
                res.status(400).send("Cannot append more deliveryBill");
            }
        }else{
            res.status(404).send("Cannot find order");
        }

    }, function (reason) {
        res.status(500).send("Something wrong in the server while saving the order.");

        // TODO: log to the error log file
        console.log(reason);
    });

});

/**
 * 订单数目
 */
router.route('/stats/count/').get(barrier.requireLogin, function(req, res){
    RepairOrder.count({}, function(err, count){
        if (err){
            res.status(500).send("服务器在统计时出错了。");
        }else{
            res.status(200).json({
                count: count
            });
        }
    });
});

/**
 * 修改调拨单接口
 */
router.route('/requisitionBill/:order_id').put(barrier.requireLogin, barrier.archivedOrderBarrier, function(req, res){
    if (!req.body.oldRequisitionBill || !req.body.newRequisitionBill || !req.params.order_id){
        return res.status(400).send("输入错误。");
    }

    var newRequisitionBill = req.body.newRequisitionBill;
    var oldRequisitionBill = req.body.oldRequisitionBill;
    RepairOrder.findById(req.params.order_id).exec().then(function(repairOrder) {
        var i;
        var modified = false;
        for (i = 0; i < repairOrder.requisitionBill.length; i++){
            var requisitionBill = repairOrder.requisitionBill[i];
            if (requisitionBill.code === oldRequisitionBill.code){
                repairOrder.requisitionBill[i] = newRequisitionBill;
                repairOrder.markModified('requisitionBill');
                modified = true;
                break;
            }
        }
        if (modified){
            saveOrder(repairOrder, "成功保存修改！", "保存失败！", res);
        }else{
            res.status(404).send("找不到调拨单！");
        }
    });
});

/**
 * 修改送货单接口
 */
router.route('/deliveryBill/:order_id').put(barrier.requireLogin, function(req, res){
    if (!req.body.oldDeliveryBill || !req.body.newDeliveryBill || !req.params.order_id){
        return res.status(400).send("Invalid input.");
    }

    var oldDeliveryBill = req.body.oldDeliveryBill;
    var newDeliveryBill = req.body.newDeliveryBill;
    RepairOrder.findById(req.params.order_id).exec().then(function(repairOrder) {
        var modified = false;

        if (repairOrder.deliveryBill.code === oldDeliveryBill.code){
            repairOrder.deliveryBill = newDeliveryBill;
            repairOrder.markModified('deliveryBill');
            modified = true;
        }

        if (modified){
            saveOrder(repairOrder, "成功保存修改！", "保存失败！", res);
        }else{
            res.status(404).send("找不到送货单！");
        }
    });
});

module.exports = router;