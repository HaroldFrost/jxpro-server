var express = require('express');
var router = express.Router();

// on routes that end in /bears ---------
// create a bear
router.route('/bears').post(function(req, res){
    var bear = new Bear();
    bear.name = req.body.name;

    bear.save(function (err) {
        if (err)
            res.send(err);

        res.json({
            message: 'Bear created!'
        });
    })
});

// get all the bear
router.route('/bears').get(function(req, res){
    Bear.find(function(err, bears){
        if (err){
            res.send(err);
        }

        res.json(bears);
    });
});

// get the bear with that id
router.route('/bears/:bear_id').get(function(req, res){
    Bear.findById(req.params.bear_id, function(err, bear){
        if (err)
            res.send(err);

        res.json(bear);
    });
});

router.route('/bears/:bear_id').delete(function(req, res){
    Bear.remove({
        _id:req.params.bear_id
    }, function (err, bear) {
        if (err)
            res.send(err);

        res.json({
            message: "Successfully deleted"
        });
    });
});

// update the bear with that id
router.route('/bears/:bear_id').put(function(req, res){
    Bear.findById(req.params.bear_id, function(err, bear){
        if (err){
            res.send(err);
        }

        bear.name = req.body.name;

        bear.save(function(err){
            if (err)
                res.send(err);

            res.json({ message: 'Bear ' + req.params.bear_id + ' updated!' });
        });
    });
});
