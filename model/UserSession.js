/**
 * Created by harold on 3/1/17.
 */
/**
 * Created by harold on 2/1/17.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var sha256 = require('sha256');
var srs = require('secure-random-string');

var UserSessionSchema = new Schema({
    staff: {type: Schema.Types.ObjectId, ref: 'Staff'},
    expireAt: Date,
    token: String
});

UserSessionSchema.index({expireAt: 1}, {expireAfterSeconds: 0});

module.exports = mongoose.model('UserSession', UserSessionSchema);