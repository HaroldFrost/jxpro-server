/**
 * Created by harold on 2/1/17.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StaffSchema = new Schema({
    name : {
        type : String,
        required : true,
        match: [/[\u4e00-\u9fa5a-zA-Z]{1,2}/, 'Invalid name']
    },
    code : {
        type: String,
        required : true,
        match : [/[0-9]{4}/, 'Invalid staff code'],
        unique: true,
        dropDups: true
    },
    password : {
        type : String,
        required : true
    },
    role : {
        type : Number,
        max : [4, 'Invalid Role Max'],
        min : [0, 'Invalid Role Min']
    }
});

module.exports = mongoose.model('Staff', StaffSchema);