/**
 * Created by harold on 4/1/17.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RepairOrderSchema = new Schema({

    client: {
        type: String,
        require: true,
        match: [/[\u4e00-\u9fa5a-zA-Z]{1,16}/, 'Invalid client name']
    },
    phone: {
        type: String,
        match: [/[0-9]{0,11}/, 'Invalid phone number']
    },
    description: String,
    servedBy:{
        type: Schema.Types.ObjectId, ref: 'Staff',
        require: true
    },
    technician: {
        type: Schema.Types.ObjectId, ref: 'Staff'
    },
    date: Date,
    paid: {type: Boolean, default: false},
    paymentDate: Date,
    paymentMethod: {
        type: String,
        enum: ['Cash', 'Wechat', 'AliPay', 'Union', 'Delivery']
    },
    passedAudit:{
        type: Boolean, default: false
    },
    archived: {type: Boolean, default: false},
    repairStatus: {
        type: Number,
        max: 1,
        min: 0,
        default: 0,
        require: true
    },
    cashier:{type: Schema.Types.ObjectId, ref: 'Staff'},
    receipt: {
        type: Number,
        min: 0,
        default: 0
    },
    quote: {
        type: Number,
        min: 0,
        default: 0
    },
    cost: {
        type: Number,
        min: 0,
        default: 0
    },
    requisitionBill: [{
        code: {
            type: String,
            match: [/[a-zA-Z0-9]{1,16}/, 'Invalid requisition code']
        },
        imgPath: {type: String}
    }],
    deliveryBill: {
        code: {
            type: String,
            match: [/[a-zA-Z0-9]{1,16}/, 'Invalid delivery code'],
            require: true
        },
        imgPath: {type: String}
    }
});

module.exports = mongoose.model('RepairOrder', RepairOrderSchema);