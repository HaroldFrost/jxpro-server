/**
 * Created by harold on 5/1/17.
 */
var UserSession = require('./model/UserSession');
var Staff = require('./model/Staff');
var Promise = require('bluebird');
var RepairOrder = require('./model/RepairOrder');

/**
 * Returns a promise which
 * Handles a request and extracts the token inside
 *
 * Checks if there is a token, if not reject the request
 *
 * Checks if the staff who sends the token, has a correct role number,
 * if not reject the request
 *
 * @param role the role number given to define the staff role
 * @param req the request sent by a user
 */
var requireRole = function(role, req){
    return new Promise(function(resolve, reject){
        var token = req.get('Authorization');
        var reason = {
            errCode: undefined,
            errorMessage: "Empty message"
        };

        // See if there is a token provided
        if (!token){
            // Give 400 if there's no token
            // res.status(400).json({
            //     message: "No token provided."
            // });
            reason.errCode = 401;
            reason.errorMessage = "No token provided.";

            // reject the promise
            reject(reason);
        }else {
            token = token.split('Bearer ')[1];
        }

        // Try to find the token in the database
        var userSessionPromise = UserSession.find({token: token}).exec();
        userSessionPromise.then(function(userSession){
            if (userSession[0]){
                // query to promise
                var staffPromise = Staff.findById({_id: userSession[0].staff}).exec();

                // try to find the staff with the id extracted from the token(actually database)
                staffPromise.then(function(staff){
                    // if found a staff && who has a higher level
                    if (staff && staff.role >= role){
                        // pass the request(promise)
                        resolve();
                    }
                    // if the staff is not found, or is at lower level
                    else{
                        reason.errCode = 403;
                        reason.errorMessage = "You don't have permission to access this endpoint.";

                        // reject the request(promise)
                        reject(reason);
                    }
                }, function(err){
                    // something wrong
                    // TODO: log to error log file
                    console.log(err);

                    reason.errCode = 500;
                    reason.errorMessage = "Something went wrong in the server when retrieving the staff";

                    // reject the request(promise)
                    reject(reason);
                });
            }
            // the token is not found on the database, maybe it's expired
            else{
                reason.errCode = 401;
                reason.errorMessage = "Token expired or invalid.";
                reject(reason);
            }
        }, function(err){
            // Something wrong
            // TODO: log to error log file
            console.log(err);

            reason.errCode = 500;
            reason.errorMessage = "Something went wrong in the server when retrieving the token";

            // reject the request(promise)
            reject(reason);
        });
    });
};

/**
 * REQUIRES LOGIN（登录拦截器）
 * middleware function to intercept unauthorized(not logged in) user to access endpoint
 */
module.exports.requireLogin = function(req, res, next){
    requireRole(0, req).then(function(){
        next();
    }, function(reason){
        res.status(reason.errCode).send(reason.errorMessage);
    });
};

/**
 * REQUIRES CASHIER（收银拦截器）
 * middleware function to intercept a staff who's not cashier(with role < 1) to access endpoint
 */
module.exports.requireCashier = function(req, res, next){
    requireRole(1, req).then(function(){
        next();
    }, function(reason){
        res.status(reason.errCode).send(reason.errorMessage);
    });
};

/**
 * REQUIRES ADMIN（管理员拦截器）
 * middleware function to intercept a staff who's not admin(with role < 2) to access endpoint
 */
module.exports.requireAdmin = function(req, res, next){
    requireRole(2, req).then(function(){
        next();
    }, function(reason){
        res.status(reason.errCode).send(reason.errorMessage);
    });
};

/**
 * Middleware to enable CORS
 * TODO: Adjust to the secure manner
 * @param req
 * @param res
 * @param next
 */
module.exports.enableCORS = function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
    next();
};

/**
 * Middleware to intercept the modifications on archived orders
 */
module.exports.archivedOrderBarrier = function(req, res, next){
    var orderId = undefined;
    if (req.params.order_id){
        orderId = req.params.order_id;
    }else if(req.body.id){
        orderId = req.body.id;
    }else{
        return res.status(400).send('Invalid input');
    }

    RepairOrder.findById(orderId).exec().then(function(repairOrder){
        if (repairOrder && !repairOrder.archived){
            next();
        }
        else{
            return res.status(400).send('Order already archived.');
        }
    });
};
